import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import { MySelectComponent } from '../../share/components/custom-form/my-select/my-select.component';

@Component({
    selector: 'app-custom-form',
    templateUrl: './custom-form.example.html',
    styles: [`label {font-weight: bold;}`],
    standalone: true,
    imports: [FormsModule, ReactiveFormsModule, MySelectComponent, JsonPipe]
})
export class CustomFormComponent implements OnInit {

  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl('joe'),
      myselect: new FormControl(2)
    });
  }

}
