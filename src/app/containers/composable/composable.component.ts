import { NgClass } from '@angular/common';
import { Component, computed } from '@angular/core';
import { useLocalStorage } from "../../share/utils/use-localstorage";

@Component({
  selector: 'app-composable',
  standalone: true,
  imports: [NgClass],
  template: `

  <div class="box">
      <div class="card bg-info">
        <div class="card-body">
            <h2>Composable</h2>
            <p>
              A "composable" in the context of an Angular application is a function which encapsulates stateful
              (outside the component) logic using the Signals API.
              The composables can be re-used in multiple components, can be nested within each other and
              can help us to organize the stateful logic of our components into small, flexible and simpler units.
            </p>
        </div>
      </div>
      <div class="p-4 d-flex gap-3 border" [ngClass]="{'bg-dark': dark(),'text-white': dark(), 'bg-light': !dark()}">
        <button class="btn btn-primary" (click)="useTheme('dark')">Use dark theme</button>
        <button class="btn btn-primary" (click)="useTheme('light')">Use light theme</button>
        <p>Stored theme: {{ storage.value() }}</p>
      </div>
  </div>
  `
})
export class ComposableComponent {
  storage = useLocalStorage('theme');
  dark = computed(() => this.storage.value() === 'dark');

  useTheme(theme: 'dark' | 'light') {
    this.storage.value.set(theme)
  }
}
