import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { TabComponent, TabsetComponent } from 'app/share/components/tabs/tabset.component';
import { CountryFlagComponent } from "../../share/components/country/country-flag.component";
import { CountrySelectedComponent } from "../../share/components/country/country-selected.component";
import { CountryComponent } from "../../share/components/country/country.component";
import { DataGridComponentType1 } from "../../share/components/data-grid-type1/data-grid-type1.component";
import { DataGridType2Component } from "../../share/components/data-grid-type2/data-grid-type2.component";
import { DataGridType3Component } from "../../share/components/data-grid-type3/data-grid-type3.component";
import { PassTemplateDirective } from "../../share/directives/pass-template.directive";
import { DataService } from './../../share/services/data.service';

@Component({
  templateUrl: "./adv-components-examples.html",
  standalone: true,
  imports: [
    CountryComponent,
    CountrySelectedComponent,
    CountryFlagComponent,
    DataGridComponentType1,
    DataGridType2Component,
    DataGridType3Component,
    PassTemplateDirective,
    TabsetComponent,
    TabComponent,
    CommonModule
  ],
})
export class AdvComponentsExampleComponent {

  countries = this.dataService.countries;
  gridData = this.dataService.gridData;
  gridConfig = this.dataService.gridConfig;
  tabs = this.dataService.tabs;
  constructor(public dataService: DataService) {}

  remove(item) {
    console.log("remove", item);
  }
  navigate(item) {
    console.log("navigate", item);
  }
}
