import { Component, ViewChild } from "@angular/core";
import { CustomIfDirective } from "../../share/directives/custom-if.directive";
import { CustomForDirective } from "app/share/directives/custom-for-directive";
import { FormsModule } from "@angular/forms";

@Component({
  selector: "app",
  templateUrl: 'custom-directives-example.html',
  standalone: true,
  imports: [CustomIfDirective, FormsModule, CustomForDirective]
})

export class CustomDirectivesExampleComponent {
  @ViewChild('addForm') addForm;
  access: boolean = true;
  people: any[] = [
    { name: 'Joe', age: 10 },
    { name: 'Patrick', age: 21 },
    { name: 'Melissa', age: 12 },
    { name: 'Kate', age: 19 }
  ];

  toggle() {
    this.access = !this.access;
  }

  remove(person) {
    let idx: number = this.people.indexOf(person);
    this.people.splice(idx, 1);
  }

  add({ value }) {
    this.people.push(value);
    this.addForm.reset();
  }
}
