import { RowLightComponent, RowDarkComponent } from './../share/components/data-grid-type1/data-grid-row.components';
import { Type } from "@angular/core";

export interface Country {
    name: string;
    code: string | null;
}

export interface GridRowModel {
    title: string,
    phone: number,
    id: number
}

export interface TabModel {
    title: string,
    content: string
}

export interface  RowTypes{
    'light': Type<RowLightComponent>,
    'dark': Type<RowDarkComponent>
}
