import { Directive, TemplateRef, ViewContainerRef, Input} from "@angular/core";

@Directive({
    selector: "[customIfDirective]",
    standalone: true,
})

export class CustomIfDirective {

    @Input() set customIfDirective(access) {
        access
            ? this.viewContainer.createEmbeddedView(this.template)
            : this.viewContainer.clear();
    }

    constructor(
        private viewContainer: ViewContainerRef,
        private template: TemplateRef<any>) {
    }
}
