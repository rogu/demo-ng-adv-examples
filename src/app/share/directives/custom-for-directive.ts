import {
    Directive,
    DoCheck,
    IterableDiffer,
    IterableDiffers,
    TemplateRef,
    ViewContainerRef,
    ViewRef,
    input
} from '@angular/core';

@Directive({
    selector: '[customRepeater]',
    standalone: true,
})

export class CustomForDirective implements DoCheck {
    private differ: IterableDiffer<any>;
    private views: Map<any, ViewRef> = new Map<any, ViewRef>();
    customRepeaterOf = input<any>();

    constructor(
        private viewContainer: ViewContainerRef,
        private template: TemplateRef<any>,
        private differs: IterableDiffers
    ) {
        this.differ = this.differs.find([]).create(null);
    }

    ngDoCheck(): void {
        let changes = this.differ.diff(this.customRepeaterOf());
        if (changes) {
            changes.forEachAddedItem((change) => {
                let view = this.viewContainer.createEmbeddedView(this.template, { '$implicit': change.item });
                this.views.set(change.item, view);
            });
            changes.forEachRemovedItem((change) => {
                let view = this.views.get(change.item);
                let idx = this.viewContainer.indexOf(view);
                this.viewContainer.remove(idx);
                this.views.delete(change.item);
            });
        }
    }
}
