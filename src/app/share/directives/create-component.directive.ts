import { ComponentRef, Directive, OnInit, ViewContainerRef, input } from '@angular/core';
import { RowTypes } from 'app/types/global';
import { RowDarkComponent, RowLightComponent } from '../components/data-grid-type1/data-grid-row.components';

const types: RowTypes = {
    'light': RowLightComponent,
    'dark': RowDarkComponent
}

@Directive({
    selector: '[createComponent]',
    standalone: true
})
export class CreateComponentDirective implements OnInit {

    createComponent = input<any>();
    data = input<any>();

    constructor(
        public container: ViewContainerRef
    ) { }

    ngOnInit(): void {
        let componentRef: ComponentRef<any> = this.container.createComponent(types[this.createComponent()]);
        componentRef.instance.data = this.data;
    }
}
