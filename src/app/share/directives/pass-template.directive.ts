import { Directive, TemplateRef } from '@angular/core';

@Directive({
    selector: '[passTemplate]',
    standalone: true
})
export class PassTemplateDirective<TImplicitContext = any> {
    constructor(public tpl: TemplateRef<{ $implicit: TImplicitContext }>) { }
}
