export function parseValue(serializedVal: string) {
    let value:any = null;
    try {
        value = JSON.parse(serializedVal);
    } catch {
        value = serializedVal;
    }

    return value;
}
