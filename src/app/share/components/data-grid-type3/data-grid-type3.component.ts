import { NgTemplateOutlet, UpperCasePipe } from '@angular/common';
import { Component, ContentChild, HostListener, input } from '@angular/core';
import { PassTemplateDirective } from '../../directives/pass-template.directive';

@Component({
  selector: 'data-grid-type3',
  template: `
        <table class="table table-hover table-bordered table-sm">
          <thead>
            @for (h of head(); track h) {
              <th>{{h|uppercase}}</th>
            }
          </thead>
          <tbody>
            @for (item of data(); track item) {
              <ng-container
                [ngTemplateOutlet]="child.tpl"
                [ngTemplateOutletContext]="{$implicit:item}">
              </ng-container>
            }
          </tbody>
        </table>
        `,
  standalone: true,
  imports: [NgTemplateOutlet, UpperCasePipe],
  exportAs: 'grid3'
})

export class DataGridType3Component {
  data = input<any[]>();
  head = input<string[]>();
  @ContentChild(PassTemplateDirective) child: PassTemplateDirective;
  selected: any;
  @HostListener('click', ['$event.target'])
  onClick(el: any) {
    const row = el.closest('tr')
    this.selected = row.id;
  }
}
