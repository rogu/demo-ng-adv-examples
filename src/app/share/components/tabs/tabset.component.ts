import { AsyncPipe } from '@angular/common';
import { AfterContentInit, Component, ContentChildren, ElementRef, QueryList, Renderer2, ViewEncapsulation, input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-tab',
  template: "<ng-content></ng-content>",
  standalone: true
})
export class TabComponent {
  title = input<string>();
  active: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private render: Renderer2, private el: ElementRef) {
    this.active
      .subscribe(result => {
        !result
          ? this.render.addClass(this.el.nativeElement, 'hide')
          : this.render.removeClass(this.el.nativeElement, 'hide');
      })
  }
}

@Component({
  selector: 'app-tabset',
  styleUrls: ["./tabset.component.css"],
  encapsulation: ViewEncapsulation.ShadowDom,
  template: `
        <div class="box">
          <ul>
            @for (tab of tabs; track tab) {
              <li
                [class.active]="tab.active | async"
                (click)="setActive(tab)">
                {{tab.title()}}
              </li>
            }
          </ul>
          <div class="content">
            <ng-content></ng-content>
          </div>
        </div>
        `,
  standalone: true,
  imports: [AsyncPipe]
})
// elements which are used between the opening and closing tags of the host element of a given
// component are called content children. It's why we use AfterContentInit class here.
export class TabsetComponent implements AfterContentInit {
  //Configures a content query.
  //Content queries are set before the ngAfterContentInit callback is called.
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  setActive(tab) {
    this.tabs.toArray().forEach(item => item.active.next(false));
    tab.active.next(true);
  }

  ngAfterContentInit() {
    this.tabs.toArray()[0].active.next(true);
  }
}
