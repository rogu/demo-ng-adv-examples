import { AsyncPipe } from '@angular/common';
import { Component, WritableSignal, effect, input, signal } from '@angular/core';
import { Country } from 'app/types/global';

@Component({
  selector: 'app-country',
  template: `
  <div style="display: flex; gap: 10px; align-items: flex-start;">
    <select #countrySelect (change)="changed(countrySelect.value)">
      <option selected disabled>select country</option>
      @for (country of countries() ; track country) {
        <option [value]="country.code">
          {{ country.name }}
        </option>
      }
    </select>
    <ng-content></ng-content>
  </div>
  `,
  standalone: true,
  imports: [AsyncPipe]
})
export class CountryComponent {
  selected: WritableSignal<string> = signal<string>('');
  countries = input<Country[]>();

  constructor() {
    effect(() => {
      console.log('selected', this.selected());
    });
  }

  changed(value: any) {
    this.selected.set(value);
  }

}
