import { Component } from '@angular/core';
import { CountryComponent } from "./country.component";
import { AsyncPipe } from '@angular/common';

@Component({
    selector: 'app-country-selected',
    template: `
  @if (country.selected(); as countryName) {
    <div>
      <b>{{countryName}}</b> is a great country!
    </div>
  }
  `,
    standalone: true,
    imports: [AsyncPipe]
})
export class CountrySelectedComponent {

  constructor(public country: CountryComponent) { }

}
