import { Component, Input } from '@angular/core';
import { CountryComponent } from "./country.component";
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-country-flag',
  template: `
  @if (country.selected(); as countryName) {
    <div>
      <img src="https://flagsapi.com/{{countryName}}/shiny/64.png" />
    </div>
  }
  `,
  standalone: true,
  imports: [AsyncPipe]
})
export class CountryFlagComponent {

  constructor(public country: CountryComponent) {

  }

}
