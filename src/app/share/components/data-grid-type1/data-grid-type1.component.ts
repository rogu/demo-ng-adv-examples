import { KeyValuePipe, UpperCasePipe } from '@angular/common';
import { Component, input } from '@angular/core';
import { CreateComponentDirective } from '../../directives/create-component.directive';

@Component({
  selector: 'app-data-grid-type1',
  template: `
        <table class="table table-hover table-sm table-bordered">
          <thead>
            <tr>@for (head of data()[0] | keyvalue; track head) {
              <th class="bg-light border">{{head.key|uppercase}}</th>
            }</tr>
          </thead>
          <tbody>
            @for (item of data(); track item) {
              <ng-container [createComponent]="rowType()" [data]="item"></ng-container>
            }
          </tbody>
        </table>
        `,
  standalone: true,
  imports: [CreateComponentDirective, KeyValuePipe, UpperCasePipe]
})
export class DataGridComponentType1 {
  rowType = input<any>();
  data = input<any[]>();
}
