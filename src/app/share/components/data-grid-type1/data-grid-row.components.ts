import { KeyValuePipe } from "@angular/common";
import { Component, input } from "@angular/core";

@Component({
    selector: 'tr[my-tr]',
    template: `
        @for (col of data() | keyvalue; track col) {
          <td>{{col.value}}</td>
        }
        `,
    standalone: true,
    imports: [KeyValuePipe]
})
export class RowLightComponent {
    data = input<any>();
}

@Component({
    selector: 'tr[my-tr].border-3',
    template: `
        @for (col of data() | keyvalue; track col) {
          <td class='bg-secondary p-1 text-white'>{{col.value}}</td>
        }
        `,
    standalone: true,
    imports: [KeyValuePipe]
})

export class RowDarkComponent {
    data = input<any>();
}
