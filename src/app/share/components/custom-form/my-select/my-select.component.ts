import { Component, forwardRef, input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-my-select',
  templateUrl: './my-select.component.html',
  styleUrls: ['./my-select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MySelectComponent),
      multi: true,
    }
  ],
  standalone: true,
  imports: []
})
export class MySelectComponent implements ControlValueAccessor {
  value: any;
  onTouch: Function;
  onModelChange: Function;
  controls = input<any[]>();

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void { }
  change(value) {
    this.onModelChange(value);
    this.writeValue(value);
  }
}
