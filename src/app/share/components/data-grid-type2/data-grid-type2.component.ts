import { NgTemplateOutlet, UpperCasePipe } from '@angular/common';
import { Component, input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-datagrid-type2',
  template: `
    <table class="table table-hover table-bordered">
      <thead>
        @if (data()) {
          <tr>
            @for (header of config(); track header) {
              <th>{{(header.header || header.key) | uppercase}}</th>
            }
            <th>ACTIONS</th>
          </tr>
        }
      </thead>
      <tbody>
        @for (model of data(); track model) {
          <tr>
            @for (item of config(); track item) {
              <td>
                {{model[item.key]}}
              </td>
            }
            <td>
              <div *ngTemplateOutlet="actionsTpl(); context: { $implicit: model }"></div>
            </td>
          </tr>
        }
      </tbody>
    </table>
    `,
  standalone: true,
  imports: [NgTemplateOutlet, UpperCasePipe]
})

export class DataGridType2Component {
  data = input<any[]>();
  config = input<any[]>();
  actionsTpl = input<TemplateRef<any>>();
}
