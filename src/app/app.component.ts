import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { RouterLinkActive, RouterLink, RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: [`.nav-link {border: 1px solid lightblue; margin: 0 3px;}`],
    standalone: true,
    imports: [RouterLinkActive, RouterLink, RouterOutlet],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppComponent { }
