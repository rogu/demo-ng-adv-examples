import { ComposableComponent } from './app/containers/composable/composable.component';
import { enableProdMode, importProvidersFrom } from '@angular/core';
import { environment } from './environments/environment';
import { enableDebugTools, BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { CustomFormComponent } from './app/containers/form-examples/custom-form.example';
import { AdvComponentsExampleComponent } from './app/containers/components-examples/adv-components-example';
import { CustomDirectivesExampleComponent } from './app/containers/directives-examples/custom-directives-example';
import { InitComponent } from './app/share/components/init/init.component';
import { provideRouter } from '@angular/router';
import { withInterceptorsFromDi, provideHttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(BrowserModule, FormsModule, ReactiveFormsModule),
    provideHttpClient(withInterceptorsFromDi()),
    provideRouter([
      { path: 'home', component: InitComponent },
      { path: 'custom-directives', component: CustomDirectivesExampleComponent },
      { path: 'adv-components', component: AdvComponentsExampleComponent },
      { path: 'custom-form', component: CustomFormComponent },
      { path: 'composables', component: ComposableComponent },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ])
  ]
})
  .then((appRef: any) => enableDebugTools(appRef));
